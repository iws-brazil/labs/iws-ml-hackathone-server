import shelve
import numpy as np
import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer
from tensorflow.keras.layers import Dropout
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences
from sklearn import preprocessing
from tensorflow.keras.models import Sequential, load_model
from tensorflow.keras.layers import Dense, Embedding, LSTM
from sklearn.model_selection import train_test_split
from tensorflow.keras.utils import to_categorical
import re
import json

import tensorflow as tf
import tensorflow.keras as keras

BEAT_SAMPLE_SIZE = 8
BEAT_SAMPLE_STEP = 1

predict_model = None

def convert_songs_dict_to_dataframe(songs):
    song_ids = []
    song_samples = []

    for song_id, song in songs.items():
        for beat_sequence in song.get('sequences', []):
            # Calculate Distance Between Notes 
            sequence_beat_distance = []
            for beat_index, beat_time in enumerate(beat_sequence):
                if beat_index > 0:
                    sequence_beat_distance.append(beat_time - beat_sequence[beat_index - 1])

            # Remove the "song tempo" factor by dividing N by N-1 
            # and creating a beat distance ratio concept (0-100)
            sequence_beat_ratios = []
            for d_index, d in enumerate(sequence_beat_distance):
                if d_index > 0:
                    prev_d = sequence_beat_distance[d_index-1]
                    sequence_beat_ratios.append(int(prev_d / d * 100))

            # Break all sequences in samples, using
            # BEAT_SAMPLE_SIZE and BEAT_SAMPLE_STEP for sampling
            with shelve.open('songs_db') as db:
                settings = db.get('settings', {})

            sampleSize = settings.get('sampleSize', BEAT_SAMPLE_SIZE)
            sampleStep = settings.get('sampleStep', BEAT_SAMPLE_STEP)

            R = sequence_beat_ratios
            sequence_samples = [R[ i : i + sampleSize] for i in range(0, len(R), sampleStep) ]

            for sample in sequence_samples:
                # Use only full samples
                if(len(sample) == sampleSize):
                    song_ids.append(str(song_id))
                    song_samples.append(sample)

    frame = {
        "songs": song_ids,
        "samples": song_samples
    }
    
    return frame


def build_model(X, Y):
    # Model Params
    batch_size = 64
    epochs = 50
    verbose = 5

    X_train, X_valid, Y_train, Y_valid = train_test_split(X, Y, test_size = 0.20, random_state = 36)

    n_timesteps, n_features, n_outputs = X_train.shape[1], X_train.shape[2], Y_train.shape[1]
    model = Sequential()
    model.add(LSTM(100, input_shape=(n_timesteps,n_features)))
    model.add(Dropout(0.5))
    model.add(Dense(100, activation='relu'))
    model.add(Dense(n_outputs, activation='softmax'))
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

    #Here we train the Network.
    model.fit(X_train, Y_train, batch_size=batch_size, epochs=epochs,  verbose=verbose)

    # Measure score and accuracy on validation set
    score, acc = model.evaluate(X_valid, Y_valid, verbose = 2, batch_size = batch_size)

    print("Logloss score: %.2f" % (score))
    print("Validation set Accuracy: %.2f" % (acc))

    return ( model, score, acc )


def train():
    global predict_model

    # Open songs from shelve
    songs = {}
    
    # with open('response.json') as json_file:
    #     data = json.load(json_file)
    #     songs = data['songs']

    # settings = {
    #     'mode': 'test',
    #     'expectedBeats': 1500,
    #     'sampleSize': 7
    # }

    with shelve.open('songs_db') as db:
        songs = db.get('songs', {})
        settings = db.get('settings', {})

    sampleSize = settings.get('sampleSize', BEAT_SAMPLE_SIZE)
    sampleStep = settings.get('sampleStep', BEAT_SAMPLE_STEP)

    # Converting into pandas dataframe and filtering only text and ratings given by the users
    df = pd.DataFrame(convert_songs_dict_to_dataframe(songs))
    df = df[['songs', 'samples']]

    samples = df['samples'].values

    X = np.array([np.array(s).reshape(sampleSize, 1) for s in samples])
    X = X.astype(float)

    encoder = preprocessing.LabelBinarizer()
    Y = encoder.fit_transform(df['songs'].values)

    # Build and train the model
    (model, score, acc) = build_model(X, Y)

    model.save("model.h5")

    predict_model = model

    return (model, float(score), float(acc), sampleSize, sampleStep)

def predict(beat_sequence):
    global predict_model
    try:
        if predict_model is None:
            predict_model = load_model("model.h5")
    except:
        return { 'error': 'Model not set. Did you train?' }
    try:
        # Calculate Distance Between Notes 
        sequence_beat_distance = []
        for beat_index, beat_time in enumerate(beat_sequence):
            if beat_index > 0:
                sequence_beat_distance.append(beat_time - beat_sequence[beat_index - 1])

        # Remove the "song tempo" factor by dividing N by N-1 
        # and creating a beat distance ratio concept (0-100)
        sequence_beat_ratios = []
        for d_index, d in enumerate(sequence_beat_distance):
            if d_index > 0:
                prev_d = sequence_beat_distance[d_index-1]
                sequence_beat_ratios.append(int(prev_d / d * 100))

        # Break all sequences in samples, using
        # BEAT_SAMPLE_SIZE and BEAT_SAMPLE_STEP for sampling
        with shelve.open('songs_db') as db:
            settings = db.get('settings', {})

        sampleSize = settings.get('sampleSize', BEAT_SAMPLE_SIZE)
        sampleStep = settings.get('sampleStep', BEAT_SAMPLE_STEP)

        R = sequence_beat_ratios
        sequence_samples= [R[ i : i + sampleSize] for i in range(0, len(R), sampleStep) ]

        samples = []
        for sample in sequence_samples:
            # Use only full samples
            if(len(sample) == sampleSize):
                samples.append(sample)

        Y = np.array([np.array(s).reshape(sampleSize, 1) for s in samples])

        P = predict_model.predict_proba(Y.astype(float))

        prediction_item = np.mean(P, axis=0)
        prediction_indexes = np.where(prediction_item == np.amax(prediction_item))

        prediction = {
            'index': int(prediction_indexes[0][0]),
            'probability': float(prediction_item[prediction_indexes[0]][0])
        }

        return prediction
    except Exception as e:
        raise e 

if __name__ == '__main__':
    train()
