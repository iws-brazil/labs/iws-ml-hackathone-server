# load and evaluate a saved model
import numpy as np 
from keras.models import load_model
 
# load model
model = load_model('model.h5')
# summarize model.
model.summary()

# evaluate the model
values = np.array([
        [
            96,
            103,
            182,
            101,
            54
        ],
        [
            103,
            182,
            101,
            54,
            110
        ],
        [
            182,
            101,
            54,
            110,
            97
        ],
        [
            50,
            301,
            178,
            18,
            198
        ],
        [
            301,
            178,
            18,
            198,
            105
        ],
        [
            178,
            18,
            198,
            105,
            95
        ],
        [
            101,
            103,
            103,
            47,
            213
        ],
        [
            103,
            103,
            47,
            213,
            92
        ],
        [
            103,
            47,
            213,
            92,
            55
        ]
    ,])

# load model
model = load_model('model.h5')
Y = model.predict_classes(values.reshape(len(values),5,1))
print(Y)

print(model.predict_proba(values.reshape(len(values),5,1)))