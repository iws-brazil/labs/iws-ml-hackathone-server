import os
from os.path import join, dirname
from dotenv import load_dotenv
from flask import Flask, json, request, Response
from flask_cors import CORS
from lstm_model import train, predict
import shelve


dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)

app = Flask(__name__)
cors = CORS(app)

@app.route('/')
def index():
    data = {}

    song_data = []
    with shelve.open('songs_db') as db:
        song_data = db.get('songs', {})
        settings = db.get('settings', {
            'mode': 'collect',
            'expectedBeats': 20,
            'sampleSize': 5
        })

        data['mode'] = settings['mode']
        data['expectedBeats'] = settings['expectedBeats']
        data['sampleSize'] = settings['sampleSize']

    for song_id, song in song_data.items():
        del song['sequences']

    data['songs'] = song_data

    js = json.dumps(data)

    return Response(js, status=200, mimetype='application/json')

@app.route('/settings', methods=['POST'])
def set_settings():
    settings = request.json

    with shelve.open('songs_db') as db:
        db['settings'] = settings

    js = json.dumps(settings)

    return Response(js, status=200, mimetype='application/json')

@app.route('/songs', methods=['GET'])
def get_songs():
    songs = []
    with shelve.open('songs_db') as db:
        songs = db.get('songs', {})

    js = json.dumps({'songs': songs})

    return Response(js, status=200, mimetype='application/json')
    

@app.route('/songs', methods=['POST'])
def post_songs():
    songs = request.json

    if songs is None:
        return Response(json.dumps([]), status=200, mimetype='application/json')

    song_dict = {}
    song_i = 0
    for song in songs:
        song_dict[song_i] = song
        song_dict[song_i]['id'] = song_i
        song_dict[song_i]['registeredBeats'] = 0
        song_dict[song_i]['sequences'] = []
        song_i = song_i + 1

    with shelve.open('songs_db') as db:
        db['songs'] = song_dict


    js = json.dumps({'songs': song_dict})

    return Response(js, status=200, mimetype='application/json')

@app.route('/songs/<id>/sequences', methods=['POST'])
def post_sequence(id):
    songs = {}
    sequence = request.json

    if sequence is None:
        return Response(json.dumps([]), status=200, mimetype='application/json')

    with shelve.open('songs_db') as db:
        songs = db['songs']
        
        songs[int(id)]['sequences'].append(sequence)
        songs[int(id)]['registeredBeats'] = songs[int(id)]['registeredBeats'] + len(sequence)
        db['songs'] = songs

    js = json.dumps({'songs': songs})

    return Response(js, status=200, mimetype='application/json')

@app.route('/train-model', methods=['POST'])
def train_model():
    (model, score, acc, sampleSize, sampleStep) = train()
    
    js = json.dumps({
        'score': score,
        'accuracy': acc,
        'sampleSize': sampleSize,
        'sampleStep': sampleStep
    })

    return Response(js, status=200, mimetype='application/json')

@app.route('/predict', methods=['POST'])
def predict_sample():
    sample = request.json

    prediction = predict(sample)

    js = json.dumps(prediction)

    return Response(js, status=200, mimetype='application/json')

if __name__ == '__main__':
    app.run(host=os.environ.get("HOST"), port=os.environ.get("PORT"))
    