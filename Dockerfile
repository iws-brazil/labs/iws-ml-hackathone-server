# base image
FROM tensorflow/tensorflow:latest-py3

# Tell Docker about the port we'll run on.
ENV CI true

ENV HOST 0.0.0.0
ENV PORT 5000
EXPOSE 5000

# Copy all local files into the image WORKDIR.
WORKDIR /usr/src/app
COPY . /usr/src/app/

RUN pip install --upgrade pip
RUN pip install -r requirements.txt

# Set the command to start the node server.
ENTRYPOINT ["python"]
CMD ["server.py"]
